#!/usr/bin/python 
#-*- coding: utf-8-*-
# server telnet 
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
# -----------------------------------------------------------------
import sys,socket
import os
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001
EOT  = chr(4)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

print os.getpid()

while True:
  try:
    conn, addr = s.accept()
  except Exception:
    continue
  while True:
    ordre = conn.recv(1024)
    if not ordre: break
    command = ["/usr/bin/%s"%(ordre)]
    pipeData = Popen(command, shell=True,stdout=PIPE, stderr=PIPE)  
    for line in pipeData.stdout:
      conn.send(line)
    for line in pipeData.stderr:
      conn.send(line)
    conn.send(EOT)
  conn.close
s.close()
sys.exit(0)
