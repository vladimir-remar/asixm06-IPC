# /usr/bin/python
#-*- coding: utf-8-*-
# exemple fork signal
# UN pare genera un fill i as mor
# El fill fa bucle infitnit, pero:
#  acaba els 3 minuts
#  amb SIGHUB torna a començar els tres minuts
#  amb SIGTERM mostra missatge(segons restants) i plega

import os,sys,signal
print "Inici I Fi del programa principal pare"

def mysighup(signum,frame):
  print 'Signal handler called with signal', signum
  print 'Time'
  signal.alarm(60*3)

def mysigterm(signum,frame):
  print 'Signal handler called with signal', signum
  segons=signal.alarm(0)
  print segons
  sys.exit(0)
  
pid = os.fork()

if pid == 0:
  print "programa fill", os.getpid(),pid
  signal.alarm(60*3)
  signal.signal(signal.SIGHUP, mysighup)
  signal.signal(signal.SIGTERM, mysigterm)
  
  while True:
    pass
else:
  sys.exit(0)
  
print "fi de programa", os.getpid(),pid
