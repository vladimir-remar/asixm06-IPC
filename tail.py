# /usr/bin/python
#-*- coding: utf-8-*-
# vladimir
# Valida la següent synopsis, on per defecte es processa stdin
# Synopsis: tail.py [-f fitxer]

import argparse
import sys

parser = argparse.ArgumentParser(description='tail: mostrar les 10 ultimas línies')
parser.add_argument('-f', dest='fitxer', default='/dev/stdin',help='fitxer a processar, default=/dev/stdin',type=str)
args = parser.parse_args()

print args
print args.fitxer

sys.exit(0)
