#!/usr/bin/python
#-*- coding: utf-8-*-
# Vladimir
# mostrar el pid
# user 1 table 1 al 10
# user 2 viscca el 155

import signal, os
import sys
PID = os.getpid()

list_num = [i for i in range(1,11)]

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  print 'Time'
  sys.exit(0)

def mydeath(signum, frame):
  print 'Signal handler called with signal', signum
  print 'Signal muerete'

def multiplica(signum,frame):
  print 'Signal handler called with signal', signum
  global list_num
  print "taulas de multiplicar"
  
  for num in range(1,10):
    print "taula del %i"%num
    for j in list_num:
      print "%i x %i = %i "%(num,j,num*j)
      
def visca(signum,frame):
  print 'Signal handler called with signal', signum
  print 'Visca el 155'
  
signal.signal(signal.SIGALRM, handler)
signal.signal(signal.SIGTERM, mydeath)
signal.signal(signal.SIGUSR1, multiplica)
signal.signal(signal.SIGUSR2, visca)
signal.alarm(60)

print PID
while True:
  pass

signal.alarm(2)
sys.exit(0)
