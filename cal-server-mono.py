#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple cal server basic mono-client
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
# -----------------------------------------------------------------
import sys,socket,os
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))
s.listen(1)
print os.getpid()

lisconec=[]
while True:
  try:
    conn, addr = s.accept()
  except Exception:
    continue
  print 'Connected by', addr
  lisconec.append(addr)
  command = ["cal"]
  pipeData = Popen(command,stdout=PIPE, stderr=PIPE,stdin=PIPE)
  for line in pipeData.stdout:
    conn.send(line)
  conn.close()
sys.exit(0)
