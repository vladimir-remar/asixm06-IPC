#Exercicis de ncat
  #a. Usant un ​ ncat ​ client contactar amb el echo-server del docker ​ hostbase ​ .
  nc 172.17.0.2 7

  #b. Usant un ​ ncat ​ client contactar amb el daytime-server del docker fet a hostbase ​ .
  nc 172.17.0.2 13

  #c. Usant un ​ ncat ​ client contactar amb un servidor web al port 80.

  echo -en "GET / HTTP/1.0\r\n\r\n" | nc localhost 80

  d. Engegar un ncat servidor multiconexió. Amb diversos clients ​ ncat contactar-hi.
  
  nc -kl 4433 #Servidor
  nc localhost 4433 #client local
  nc 192.168.2.31 4433 #client de la xarxa local
  nc 172.17.0.1 4433 #client docker

3. Monitorització de tràfic de xarxa.
  #a. Observar el funcionament de la ordre ss i netstat.
  #b. Observar el funcionament de la ordre iptraf.
  #c. Monitoritzar el tràfic de xarxa (entre un client ncat i un servidor) amb wireshark.
    
