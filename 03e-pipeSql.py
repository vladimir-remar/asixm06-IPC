#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# Vladimir
# Consultar dades que tenim a Postgres mitjançant una pipe amb PSQL.
# Select * from repventas.
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# multiples consultas
# ##########################################
'''
import sys
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser(description='consulta sql interactiva')
parser.add_argument('-c',dest='numempl', help='sentencia sql a executar', metavar='sentencia sql', action='append', required=True)
args = parser.parse_args()
cmd = "psql -qt -h 172.17.0.2 -U postgres training"
pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

for user in args.numempl:
  sqlStatment = 'select * from repventas where num_empl=%s;\n'%(user)
  pipeData.stdin.write(sqlStatment)
  data = pipeData.stdout.readline()
  if len(data) != 1:
    print data,
    data = pipeData.stdout.readline()
  else:
    sys.stderr.write("empleat:%s not found\n" % (user))
    
pipeData.stdin.write('\q\n')
sys.exit(0)
