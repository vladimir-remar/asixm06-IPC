#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# Vladimir
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisix2 M06-ASO UF2NF1-Scripts
# Telnet server multiple-connexions
# -----------------------------------------------------------------
'''
import socket, sys, select
from subprocess import Popen, PIPE
HOST = ''                 
PORT = 50001
EOT  = chr(4)             
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
conns=[s]


while True:
  actius,x,y = select.select(conns,[],[])
  for actual in actius:
    if actual == s:
      conn, addr = s.accept()
      sys.stderr.write('Connected by %s,%s'%(addr))
      conns.append(conn)
    else:
      ordre = actual.recv(1024)
      if not ordre:
        sys.stdout.write("Client finalitzat: %s \n" % (actual))
        actual.close()
        conns.remove(actual)
        continue
      else:
        command = ["/usr/bin/%s"%(ordre)]
        pipeData = Popen(command, shell=True,stdout=PIPE, stderr=PIPE)  
        for line in pipeData.stdout:
          actual.sendall(line)
        for line in pipeData.stderr:
          actual.sendall(line)
        actual.sendall(EOT,socket.MSG_DONTWAIT)
s.close()
sys.exit(0)



