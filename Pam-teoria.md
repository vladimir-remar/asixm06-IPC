# Modelo de funcionamiento de PAM

1. Identificar las aplicaciones que utilizan PAM
   
        ls /etc/pamd.d
    
2. Identificar los ficheros de configuracion de PAM de las aplicaciones 
y entender el funcionamiento

        vim /etc/pam.d/chfn #En este caso la aplicacion chfn sirve para
        modificar datos de los usuarios (GECOS)
  
3. Elementos de la configuracion:
Cada line de configuracion (llamada regla) en el fichero PAM de un 
servicio tiene la forma:

    Type control module-path module-arguments

    - Type
    - Control
    - Modulo
    - Opciones

    Identificar la funcionalidad de cada type.
    
      - **auth:**Autentifica a un usuario y configura sus 
      credenciales. Por lo general, esto se debe a alguna solicitud de 
      respuesta al desafío que el usuario debe cumplir: si usted es 
      quien dice ser, por favor, introduzca su contraseña.
      - **account:** Proporcionar tipos de servicio de verificación 
      de cuenta: ¿ha expirado la contraseña del usuario? ¿este 
      usuario tiene acceso permitido al servicio solicitado?
      - **password:** la responsabilidad de este grupo es la tarea de 
      actualizar los mecanismos de autenticación. Por lo general, 
      dichos servicios están estrechamente relacionados con los del 
      grupo auth.
      - **session:** Este grupo de tareas cubre cosas que deberían 
      hacerse antes de que se dé un servicio y después de que se 
      retire. Tales tareas incluyen el mantenimiento de pistas de 
      auditoría y el montaje del directorio de inicio del usuario.
4. Entender el funcionamiento de los controlses basicos:

    - **requisite:** Es como el `required` pero el control es devuelto
    directamente a la aplicacion sin haber revisado el resto de modulos. 
    - **required:** El fallo de este control dara como resultado un 
    fallo y la devolucion al control de la API no sin antes haber 
    revisado el resto de modulos.
    - **sufficient:** Si el control analizado ha tenido exito y si se ha 
    llevado un control previo con exito devolvera sin haber revisado el 
    resto de modulos un `success` sin embargo si el control previo fallo
    continura analizando los siguientes modulos ademas si el actual control
    falla tambien continuara analizando el resto de modulos.
    - **optional:** El exito o fallo de este modulo solo es importante si 
    este es el unico modulo en el `stack` asociado con este servicio.
    - **inlcude:** Incluye todas las lineas de un fichero de configuracion
    espificado como un argumento de este control.
    - **substack:** Es como el `include` pero difiere en como se evalua 
    las acciones `done` y `die`, es decir si algo falla dentro del substack
    este continua con los modulos fuera del substack.
    
    Identificar los otros controles avanzados
    **[value1=action1 value2=action2]**
    
    - **ignore:** Cuando es usado con un stack de modulos, los modulos
    devuleven un estado con no contribuira con el return final que obtenga
    la aplicacion.
    - **bad:** Esta accion indica que el codigo `return` debe considerarse
    como indicativo que el modulo ha fallado.
    - **ok:** si el modulo es success este se sumara a la pila ya almacenada
    de returns previos, sin embargo si la pila lleva un fail este no sobreescribira
    el valor previo, pero continurara con los siguientes modulos.
    - **done:** es equivalente al `ok` pero con el defecto que si tiene exito 
    devuelve el control inmediado a la aplicacion sin haber ejecutado el 
    resto de modulos.
    - **N(an unsigned integer):** equivalente a ok, con el efecto de saltar
    modules segun se haya indicado en el numero.
    - **reset:** limpia toda la memoru de el estado de el stack del modulo
    e inicia otra vez con el siguiente modulo.
    
    Equivalencias para: `requisite,required,sufficient,optional`
    
    - **required** [success=ok new_authok_reqd=ok ignore=ignore defauld=bad]
    - **requisite** [success=ok new_authok_reqd=ok ignore=ignore defauld=die]
    - **sufficient** [success=done new_authok_reqd=ok  defauld=ignore]
    - **optional** [success=ok new_authok_reqd=ok  defauld=ignore]
5. Consultar la ubicacion y el man de los modulos pam_`<name>`.so 
utilizados en las configuraciones PAM

  
