# /usr/bin/python
#-*- coding: utf-8-*-
# exemple fork signal
# UN pare genera un fill i as mor
# El fill fa bucle infitnit, pero:
#  acaba els 3 minuts
#  amb SIGHUB torna a començar els tres minuts
#  amb SIGTERM mostra missatge(segons restants) i plega

import os,sys,signal
print "Inici del programa principal pare"

pid = os.fork()

if pid !=0:
  print 'Adeu pare'
  sys.exit(0)
print "programa fill", os.getpid(),pid
os.execv('/usr/bin/python',['/usr/bin/python','/home/users/inf/hisx2/isx48262276/Documents/M06/IPC/06-signal.py','3'])

