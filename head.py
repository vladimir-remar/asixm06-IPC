# /usr/bin/python
#-*- coding: utf-8-*-
# vladimir
# Valida la següent synopsis, on per defecte es processa stdin.
# Synpsis: $ head [file]
# ----------------------------------------------------------------------
import argparse
import sys

parser = argparse.ArgumentParser(description='head: mostrar les 10 primeras línies')
parser.add_argument(dest='fitxer', default='/dev/stdin', help='fitxer a processar, default=/dev/stdin',type=str)

args = parser.parse_args()
print args
print args.fitxer

sys.exit(0)
