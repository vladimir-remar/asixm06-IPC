# /usr/bin/python
#-*- coding: utf-8-*-
# vladimir
# Valida la següent synopsis, on per defecte es processen les 10 últimes línies de
# stdin.
# Synopsis: tail.py [-f fitxer] [-n línies].
# ----------------------------------------------------------------------

import argparse
import sys

parser = argparse.ArgumentParser(description='tail: mostrar les últimes n línies')

parser.add_argument('-f', dest='fitxer', default='/dev/stdin',help='fitxer a processar, default=/dev/stdin',type=str)

parser.add_argument('-n', dest='lines', default=10,type=int,help='número de línies a mostar',metavar='numero-lines', choices=[5,10,15])

args = parser.parse_args()

print args
print args.fitxer
print args.lines

sys.exit()
