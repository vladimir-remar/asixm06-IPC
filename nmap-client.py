#!/usr/bin/python 
#-*- coding: utf-8-*-
# nmap Client
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
# -----------------------------------------------------------------
import sys,socket,os,argparse,time
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description='Client: sonsulta calendar')
parser.add_argument('-t',dest='host', help='opcio target ', metavar='target', default='localhost')
args = parser.parse_args()

HOST = ''
PORT = 50001
target = args.host

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

command = ["/usr/bin/nmap  %s"%(target)]
pipeData = Popen(command, shell=True,stdout=PIPE, stderr=PIPE)

for line in pipeData.stdout:
  s.send(line)
s.close()
sys.exit(0)
