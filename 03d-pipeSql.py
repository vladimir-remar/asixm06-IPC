#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# Consultar dades que tenim a Postgres mitjançant una pipe amb PSQL.
# Select * from clientes.
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# d) sql injectat ------> FATAL
# ##########################################
'''

import sys
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser(description='consulta sql interactiva')
parser.add_argument('numempl', help='sentencia sql a executar', metavar='sentencia sql')
args = parser.parse_args()

cmd = "psql -h 172.17.0.2 -U postgres training"
sqlStatment = 'select * from repventas where num_empl=%s'%(args.numempl)
pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write(sqlStatment+'\n\q\n')

for line in pipeData.stdout:
	print line,
sys.exit(0)
