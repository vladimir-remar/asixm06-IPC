#!/usr/bin/python 
#-*- coding: utf-8-*-
# exemple de echo client basic
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# @edt Curs 2017-2018
# Vladimir remar
# -----------------------------------------------------------------
import sys,socket
HOST = ''
PORT = 50001
EOT  = chr(4)#man ascii: end of transmission

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

while True:
  dades=raw_input()
  if dades == 'fi':
    #Asi es como el cliente "cuelga"
    break 
  s.sendall(dades)
  #python docs 17.2.1 >>socket.socket.sendall
  #A diferencia del send continua enviando datos hasta que se envien todos o ocurra un error
  data = s.recv(1024)
  if data is None:
    break
  print data
  
s.close()
sys.exit(0)

